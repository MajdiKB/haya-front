import ResultDataModel from "./ResultDataModel";

export default interface ListApiResponseModel {
  config: {};
  data: ResultDataModel;
  headers: {};
  request: XMLHttpRequest;
  status: number;
  statusText: string;
}

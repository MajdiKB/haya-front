import axios from "axios";
import ListApiResponseModel from "@/Models/ListApiResponseModel";
const API: string = process.env.VUE_APP_URL_API;

export default {
  get(page: string): Promise<ListApiResponseModel> {
    return axios.get(API + "houses?page=" + page);
  },
};

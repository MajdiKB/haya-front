import axios from "axios";
import AllItemsApiResponseModel from "../Models/AllItemsApiResponseModel";

const API: string = process.env.VUE_APP_URL_API;

export default {
  get(): Promise<AllItemsApiResponseModel> {
    return axios.get(API + "allHouses");
  },
};
